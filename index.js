const express = require("express");
const cors = require("cors");
const { randomUUID } = require("crypto"); // Added in: node v14.17.0
const bodyParser = require("body-parser");
const Keyv = require("keyv");
const jwt = require("jsonwebtoken");
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
const keyv = new Keyv(`sqlite://${__dirname}/database.sqlite`, {
  serialize: JSON.stringify,
  deserialize: JSON.parse,
});
app.get("/",(req,res)=>res.json({msg:"Hello"}))
app.post("/auth/signup", async function (req, res, next) {
  const { username, password } = req.body;
  let users = await keyv.get("users", []);
  if (!users) {
    users = [];
  }
  const found = users.find((item) => item.username === username);
  if (found) {
    res.status(422).json({ msg: "Username already taken" });
    return;
  }
  users.push({ username, password, id: randomUUID() });
  keyv.set("users", users);
  res.json({
    message: "Success",
    username,
  });
});

app.post("/auth/signIn", async function (req, res, next) {
  const { username, password } = req.body;
  let users = await keyv.get("users", []);
  if (!users) {
    users = [];
  }
  const user = users.find(
    (item) => item.username === username && item.password === password
  );
  if (!user) {
    res.status(422).json({ msg: "Invalid user" });
    return;
  }
  const token = jwt.sign({ id: user.id }, "KEY_REACT_APP", {
    expiresIn: "8h",
  });
  return res.json({
    user,
    token,
  });
});

app.get("/todo/get", async function (req, res, next) {
  const token = req.body.token || req.query.token || req.headers["auth"];
  let users = await keyv.get("users", []);
  if (!token) {
    res.status(401).json({ msg: "Unauthorized" });
  } else {
    const decoded = jwt.verify(token, "KEY_REACT_APP");
    const { id } = decoded;
    const found = users.find((item) => item.id === id);
    let todosState = await keyv.get(`todo-${found.id}`);
    if (!todosState) {
      todosState = {};
    }
    res.json({
      todosState,
      mgs: "Success",
    });
    return;
  }
});

app.get("/todo/save", async function (req, res, next) {
  const token = req.body.token || req.query.token || req.headers["auth"];
  const todoDataset = req.body.todoState;
  let users = await keyv.get("users", []);
  if (!token) {
    res.status(401).json({ msg: "Unauthorized" });
  } else {
    const decoded = jwt.verify(token, "KEY_REACT_APP");
    const { id } = decoded;
    const found = users.find((item) => item.id === id);
    let todosState = await keyv.set(`todo-${found.id}`, todoDataset);
    res.json({
      todosState,
      msg: "Saved",
    });
    return;
  }
});

app.listen(3500, function () {
  console.log("CORS-enabled web server listening on port 3500");
});
